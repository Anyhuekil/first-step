// address value throw json : {address: lat: lng:}
function doFunction() {
	var nativeGeocord = new Array();
	var data = new Object();
	data.name = properties.name;
	data.locality = properties.locality;
	data.country = properties.country;
	data.postalcode = properties.postalcode ? properties.postalcode : '';
	data.street = properties.street ? properties.street : '';
	data.lat = mapLat.toFixed(4);
	data.lng = mapLng.toFixed(4);
	nativeGeocord.push(data);
	var jsonData = JSON.stringify(nativeGeocord);
	return jsonData;
};
doFunction();