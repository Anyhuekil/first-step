function doFunction() {
	var nativeGeocord = new Array();
	var data = new Object();
	if(addressComponents && mapLat && mapLng){
		data.street_number = addressComponents.address_components[1].short_name;
		data.route = addressComponents.address_components[2].short_name;
		data.locality = addressComponents.address_components[4].short_name;
		data.political = (addressComponents.address_components[6] ? addressComponents.address_components[6].short_name : '');
		data.postal_code = (addressComponents.address_components[8] ? addressComponents.address_components[8].short_name : '');
		data.country = (addressComponents.address_components[7] ? addressComponents.address_components[7].short_name : '');
		data.lat = mapLat;
		data.lng = mapLng;
		nativeGeocord.push(data);
	}
	var jsonData = JSON.stringify(nativeGeocord);
	
	return jsonData;
};
doFunction();